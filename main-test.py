import hashlib

password = 'Pa$$w0rD'.encode()
salt = 'Ваша соль'.encode()
dk = hashlib.pbkdf2_hmac('sha256', password, salt, 100000)
dk.hex()
